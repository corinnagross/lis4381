# Lis4381 Mobile WebApp Development

## Corinna Gross

### Lis4381 Requirements:

*Three Parts:*

1. Screenshots of A5 server-side validation
    - Screenshots of failed validation
    - Screenshots of successful validation
2. Screenshot of Skillsets


#### Online Portfolio Server-Side Data Validation:

*A5 Main Page:
![Skillset 13 Screenshot](img/a5_index.png)

| *Screenshot of Failed Data Validation: |       |
|:--------------------------------|---------------------------------|
![Invalid Data Validation Screenshot](img/a5_invalid.png) | ![Failed Data Validation Screenshot](img/a5_failed_validation.png) 

| *Screenshot of Successful Data Validation: |       |
|:--------------------------------|---------------------------------|
![Valid Data Validation Screenshot](img/a5_valid.png) | ![Passed Data Validation Screenshot](img/a5_passed_validation.png) 


#### Skillsets

*Screenshot of Skillset 13 - Sphere Volume Calculator:

![Skillset 13 Screenshot](img/Q13_screenshot.png)


|*Screenshot of Skillset 14 - Simple Calculator: |              |
| ------------------------------------------------ | ---------------------------------------------------- | 
| ![Skillset 14 Screenshot 1](img/simple_calc_ss1.png) | ![Skillset 14 Screenshot 2](img/simple_calc_ss2.png)  
| ![Skillset 14 Screenshot 3](img/simple_calc_ss3.png) | ![Skillset 14 Screenshot 4](img/simple_calc_ss4.png)  


|*Screenshot of Skillset 15 - Read/Write File: |              |
| ------------------------------------------------ | ---------------------------------------------------- | 
| ![Skillset 15 Screenshot 1](img/rwf_ss1.png) | ![Skillset 15 Screenshot 2](img/rwf_ss2.png) 