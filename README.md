> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# Lis4381 Mobile WebApp Development

## Corinna Gross

### Lis4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
        - Screenshot of AMPPS running
    - Install Java Developer Kit
        - Screenshot of JDK java "Hello"
    - Install Visual Studio Code
    - Install Android Studio
        - Screenshot of Android Studio "My First App"
    - Provide git command descriptions
    - Create Bitbucket Repository
    - Complete Bitbucket tutorials (bitbucketstationlocations)


2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Develop an application that shows a recipe
        - Screenshot of application runnning
    - Skill set 1: Even or Odd application
        - Screenshot of skill set 1 running
    - Skill set 2: Largest of Two Integers application
        - Screenshot of skill set 2 running
    - Skill set 3: Arrays and Loops application
        - Screenshot of skill set 3 running

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create a database using mySQL
        - Screenshot of 10 records in each ERD table
        - Link to mySQL mbw and sql files     
    -  Create an app to calculate ticket price using Android Studio
        - Screenshots of  running application's opening user interface
        - Screenshots of  running application's processing user input
    - Skill set 4: Decision Structures application
        - Screenshot of skill set 4 running
    - Skill set 5: Random Number Generator application
        - Screenshot of skill set 5 running
    - Skill set 6: Methods application
        - Screenshot of skill set 6 running

4. [P1 README.md](p1/README.md "My P1 README.md file")
    -  Create an app of my digital business card
        - Screenshots of  running application's opening user interface
        - Screenshots of  running application's processing user input
    - Skill set 7: Random Number Generator Data Validation
        - Screenshot of skill set 7 running
    - Skill set 8: Largest of Three Numbers
        - Screenshot of skill set 8 running
    - Skill set 9: Array Runtime Data Validation
        - Screenshot of skill set 9 running

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create an Online Portfolio
        - Screenshot of Main Page 
    -  Create working client-side data validation on Online Portfolio
        - Screenshots of failed data validation
        - Screenshot of sucessful data validation
    - Skill set 10: Array List
        - Screenshot of skill set 10 running
    - Skill set 11: Alpha Numeric Special
        - Screenshot of skill set 11 running
    - Skill set 12: Temperature Coonversion
        - Screenshot of skill set 12 running

6. [A5 README.md](a5/README.md "My A5 README.md file")
    -  Create working server-side data validation on Online Portfolio
        - Screenshots of failed data validation
        - Screenshot of sucessful data validation
    - Skill set 13: Sphere Volume Calculator
        - Screenshot of skill set 13 running
    - Skill set 14: Simple Calculator
        - Screenshot of skill set 14 running
    - Skill set 15: Read/Write File
        - Screenshot of skill set 15 running

7. [P2 README.md](p2/README.md "My P2 README.md file")
    -  Show working server-side data validation and prepared statements to help prevent SQL injection.
    - Screenshots of working Edit/Delete functionalities 
    - Create link to RSS feed



    