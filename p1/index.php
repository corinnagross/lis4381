<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="An online portfolio that highlights some of my projects and skills aquired relating to Information, Communication, and Technology.">
		<meta name="author" content="Corinna Gross">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Project 1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> 
					<p>1. Screenshots of running android studio app: Digital Business Card</p>
					<p>  - Screenshots of application's opening user interface</p>
					<p>  - Screenshots of application's processing user input</p>
					<p>2. Screenshot of Skillsets</p

				<h4>Business Card App Screenshots:</h4>
				<img src="img/Ss1.png" class="img-responsive center-block" alt="Business Card App Opening Interface Screenshot">
				<img src="img/Ss2.png" class="img-responsive center-block" alt="Business Card App Processing Interface Screenshot">
				
				<h4>Skillset 7 - Random Number Generator Data Validation:</h4>
				<img src="img/Q7screenshot.png" class="img-responsive center-block" alt="Skillset 7 Screenshot">
				
				<h4>Skillset 8 - Largest of Three Numbers:</h4>
				<img src="img/Q8screenshot.png" class="img-responsive center-block" alt="Skillset 8 Screenshot">
				
				<h4>Skillset 9 - Array Runtime Data Validation: </h4>
				<img src="img/Q9screenshot.png" class="img-responsive center-block" alt="Skillset 9 Screenshot">

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
