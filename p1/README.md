> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis4381 Mobile WebApp Development

## Corinna Gross

### Lis4381 Requirements:


1. Screenshots of running android studio app: Digital Business Card
    - Screenshots of application's opening user interface
    - Screenshots of application's processing user input
2. Screenshot of Skillsets


#### Business Card App Screenshots:

| *Screenshot of App opening user interface: | *Screenshot of App processing user input: |
|:--------------------------------|---------------------------------|
![Business Card App Opening Interface Screenshot](img/Ss1.png) | ![Business Card App Processing Interface Screenshot](img/Ss2.png) 



#### Skillset Screenshots:

| *Screenshot of Skillset 7 - Random Number Generator Data Validation: | *Screenshot of Skillset 8 - Largest of Three Numbers: | *Screenshot of Skillset 9 - Array Runtime Data Validation: |
| ------------------------------------------------ | ---------------------------------------------------- | ------------------------------------ |
| ![Skillset 7 Screenshot](img/Q7screenshot.png) | ![Skillset 8 Screenshot](img/Q8screenshot.png) | ![Skillset 9 Screenshot](img/Q9screenshot.png) |
