<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
//exit(print_r($_POST));

//exit(print_r($_POST)); //display $_POST array values from form

// or, for nicer display in browser...
 //echo "<pre>";
 //print_r($_POST);
 //echo "</pre>";
 //exit(); //stop processing, otherwise, errors below



//include('index.php'); //forwarding is faster, one trip to server
//header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)


?> 
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta https-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="An online portfolio that highlights some of my projects and skills aquired relating to Information, Communication, and Technology.">
	    <meta name="author" content="Corinna Gross">
	    <link rel="icon" href="favicon.ico">

        <title>LIS4381 - Simple Calculator</title>
        <?php include_once("../css/include_css.php"); ?>
   </head>

 <body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>

			 <h3>Performs Addition, Subtraction, Multiplication, Division, and Exponentiation</h3>
			 <h2>Perform Calculation</h2>


            <?php
            if (!empty($_POST))
            {
                $num1 = $_POST['num1'];
                $num2 = $_POST['num2'];
                $operation = $_POST['operation'];
            

             if (is_numeric($num1) && is_numeric($num2))
                {
                    //exit(print_r($_POST));

                    echo'<h2>'."$operation".'</h2>';

                    function AddNum($x, $y)
                    {
                        echo "$x"." + "."$y"." = ";
                        echo $x + $y;
                    }
                    function SubtractNum($x, $y)
                    {
                        echo "$x"." - "."$y"." = ";
                        echo $x - $y;
                    }
                    function MultiplyNum($x, $y)
                    {
                        echo "$x"." * "."$y"." = ";
                        echo $x * $y;
                    }
                    function DivideNum($x, $y)
                    {
                        if($y == 0)
                        {
                            echo "Cannot divide by zero!";
                        }
                        else
                        {
                            echo "$x"." / "."$y"." = ";
                            echo $x / $y;
                        }
                    }
                    function PowerNum($x, $y)
                    {
                        echo "$x"." raised to the power of "."$y"." = ";
                        echo pow($x,$y);
                    }

                    //call functions
                    if($operation == 'addition')
                    {
                        AddNum($num1, $num2);
                    }
                    else if($operation == 'subtraction')
                    {
                        SubtractNum($num1, $num2);
                    }
                    else if($operation == 'multiplication')
                    {
                        MultiplyNum($num1, $num2);
                    }
                    else if($operation == 'division')
                    {
                        DivideNum($num1, $num2);
                    }
                    else if($operation == 'exponentiation')
                    {
                        PowerNum($num1, $num2);
                    }
                    else
                    {
                        echo "Must select an operation.";
                    }

                    ?>
                 <p>
                  <?php

                }//end preg_match if

                    else
                    {
                        echo "Must enter valid number";
                    }

                    }//end if(!empty($_POST))

                    else
                    {
                        header('Location: index.php');
                    }
                 ?>
                 </p>

            <?php include_once "global/footer.php"; ?>
        
       </div><!--starter template-->
    </div><!--end container-->

<?php include_once("../js/include_js.php"); ?>
*<script>
$(document).ready(function(){
	$('#myTable').DataTable({
		responsive: true
	});
});
</script>

</body>
</html>

