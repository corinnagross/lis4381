<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="An online portfolio that highlights some of my projects and skills aquired relating to Information, Communication, and Technology.">
		<meta name="author" content="Corinna Gross">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> 
					<p>1. Create a mobile recipe app using Android Studio</p>
					<p>2. Screenshot of running applications first user interface</p>
					<p>3. Screenshots of skillsets</p>

				<h4>Mobile Recipe App Screenshots:</h4>
				<img src="img/recipe_app_interface1.png" class="img-responsive center-block" alt="Recipe App Interface 1">
				<img src="img/recipe_app_interface2.png" class="img-responsive center-block" alt="Recipe App Interface 2">

				<h4>Skillset 1- Even or Odd application:</h4>
				<img src="img/even_or_odd_screenshot.png" class="img-responsive center-block" alt="Skillset 1 Screenshot">

				<h4>Skillset 2 - Largest of Two Integers application:</h4>
				<img src="img/largest_number_screenshot.png" class="img-responsive center-block" alt="Skillset 2 Screenshot">
				
				<h4>Skillset 3 - Arrays and Loops Application:</h4>
				<img src="img/arrays_loops_screenshot.png" class="img-responsive center-block" alt="Skillset 3 Screenshot">
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
