
# Lis4381 Mobile WebApp Development

## Corinna Gross

### Lis4381 Requirements:

1. Create a mobile recipe app using Android Studio
2. Screenshot of running applications first user interface
3. Screenshots of skillsets 

#### Mobile Recipe App Screenshots:

| *Screenshot of App interface 1: | *Screenshot of App interface 2: |
|:--------------------------------|---------------------------------|
![Recipe App Interface 1 Screenshot](img/recipe_app_interface1.png) | ![Recipe App Interface 2 Screenshot](img/recipe_app_interface2.png) 


#### Assignment Skillset Screenshots:

*Screenshot of Skillset 1 - Even or Odd application:

![Skillset 1 Screenshot](img/even_or_odd_screenshot.png)

*Screenshot of Skillset 2 - Largest of Two Integers application:

![Skillset 2 Screenshot](img/largest_number_screenshot.png)

*Screenshot of Skillset 3 - Arrays and Loops Application:

![Skillset 3 Screenshot](img/arrays_loops_screenshot.png)
