# Lis4381 Mobile WebApp Development

## Corinna Gross

### Lis4381 Requirements:

1. Screenshot of mySQL ERD
2. Screenshot of 10 records in each mySQL table
3. Link to mySQL mbw and sql files
4. Create a ticket price calculating app using Android Studio
5. Screenshots of running application's opening user interface
6. Screenshots of running application's processing user input
7. Screenshot of Skillsets


#### mySQL Screenshots:

*Screenshot of mySQL ERDs:

![mySQL ERDs Screenshot](img/ERD_screenshot.png) 


| *Screenshot of 10 records: |
![mySQL Pet Table Records Screenshot](img/pet_records.png) | ![mySQL Petstore Table Records Screenshot](img/petstore_records.png) | ![mySQL Petstore Table Records Screenshot](img/customer_records.png) 

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")
[A3 SQL File](docs/a3.sql "A# SQL Script") 

#### Mobile App Screenshots:

| *Screenshot of App opening user interface: | *Screenshot of App processing user input: |
|:--------------------------------|---------------------------------|
![Ticket App Opening Interface Screenshot](img/ticketsapp_opening.png) | ![Ticket App Opening Interface Screenshot](img/ticketsapp_processing.png) 

#### Assignment Skillset Screenshots:

| *Screenshot of Skillset 4 - Decision Structures: | *Screenshot of Skillset 5 - Random_Number Generator: | *Screenshot of Skillset 6 - Methods: |
| ------------------------------------------------ | ---------------------------------------------------- | ------------------------------------ |
| ![Skillset 4 Screenshot](img/Q4_decision_structures_screenshot.png) | ![Skillset 5 Screenshot](img/Q5.png) | ![Skillset 6 Screenshot](img/Q6_methods_screenshot.png) |


