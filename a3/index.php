<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="An online portfolio that highlights some of my projects and skills aquired relating to Information, Communication, and Technology.">
		<meta name="author" content="Corinna Gross">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> 
					<p>1. Screenshot of mySQL ERD</p>
					<p>2. Screenshot of 10 records in each mySQL table</p>
					<p>3. Link to mySQL mbw and sql files</p>
					<p>4. Create a ticket price calculating app using Android Studio</p>
					<p>5. Screenshots of running application's opening user interface</p>
					<p>6. Screenshots of running application's processing user input</p>
					<p>7. Screenshot of Skillsets</p>

				<h4>Screenshot of mySQL ERDs:</h4>
				<img src="img/ERD_screenshot.png" class="img-responsive center-block" alt="mySQL ERDs screenshot">

				<h4>Screenshot of 10 Records:</h4>
				<img src="img/pet_records.png" class="img-responsive center-block" alt="mySQL Pet Table Records Screenshot">
				<img src="img/petstore_records.png" class="img-responsive center-block" alt="mySQL Petstore Table Records Screenshot">
				<img src="img/customer_records.png" class="img-responsive center-block" alt="mySQL Customer Table Records Screenshot">

				<h4>Mobile App Screenshots:</h4>
				<img src="img/ticketsapp_opening.png" class="img-responsive center-block" alt="mySQL Pet Table Records Screenshot">
				<img src="img/ticketsapp_processing.png" class="img-responsive center-block" alt="mySQL Pet Table Records Screenshot">
				
				<h4>Skillset 4 - Decision Structures: </h4>
				<img src="img/Q4_decision_structures_screenshot.png" class="img-responsive center-block" alt="Skillset 4 Screenshot">
				
				<h4>Skillset 5 - Random_Number Generator: </h4>
				<img src="img/Q5.png" class="img-responsive center-block" alt="Skillset 5 Screenshot">
				
				<h4>Skillset 6 - Methods:</h4>
				<img src="img/Q6_methods_screenshot.png" class="img-responsive center-block" alt="Skillset 6 Screenshot">

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
