-- MySQL Script generated by MySQL Workbench
-- Tue Feb  8 17:56:41 2022
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema clg19
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `clg19` ;

-- -----------------------------------------------------
-- Schema clg19
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `clg19` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
SHOW WARNINGS;
USE `clg19` ;

-- -----------------------------------------------------
-- Table `clg19`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clg19`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `clg19`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT UNSIGNED NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `clg19`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clg19`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `clg19`.`customer` (
  `cust_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cust_fname` VARCHAR(15) NOT NULL,
  `cust_lname` VARCHAR(30) NOT NULL,
  `cust_street` VARCHAR(30) NOT NULL,
  `cust_city` VARCHAR(30) NOT NULL,
  `cust_state` CHAR(2) NOT NULL,
  `cust_zip` INT UNSIGNED NOT NULL,
  `cust_phone` BIGINT UNSIGNED NOT NULL,
  `cust_email` VARCHAR(100) NOT NULL,
  `cust_balance` DECIMAL(8,2) NOT NULL,
  `cust_total_sales` DECIMAL(8,2) NOT NULL,
  `cust_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cust_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `clg19`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clg19`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `clg19`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cust_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_customer_idx` (`cust_id` ASC) VISIBLE,
  INDEX `fk_pet_petstore1_idx` (`pst_id` ASC) VISIBLE,
  CONSTRAINT `fk_pet_customer`
    FOREIGN KEY (`cust_id`)
    REFERENCES `clg19`.`customer` (`cust_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_petstore1`
    FOREIGN KEY (`pst_id`)
    REFERENCES `clg19`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `clg19`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `clg19`;
INSERT INTO `clg19`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Claws and Paws', '123 Main St.', 'Hollywood', 'CA', 900195341, 3647686544, 'clwsandpws@gmail.com', 'http://www.clawsandpaws.com', 50000, NULL);
INSERT INTO `clg19`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Furry Friends', '1435 Chase Dr.', 'Chicago', 'IL', 618959031, 8785654341, 'furryfriends2@gmail.com', 'http://www.furryfriendspetstore.com', 65000, NULL);
INSERT INTO `clg19`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Partners', '811 4th St.', 'Nashville', 'TN', 120056728, 9096834657, 'petpartners@yahoo.com', 'http://www.petpartner.com', 72000, NULL);
INSERT INTO `clg19`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Palace', '18903 Tennessee St.', 'Tallahassee', 'FL', 328280022, 2135796582, 'palacepets@gmail.com', 'http://www.petpalace.org', 87000, NULL);
INSERT INTO `clg19`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Purrs and Gurrs', '751 Lois Lane', 'Phoenix', 'AZ', 324077343, 8684549274, 'purrsgurrss@aol.com', 'http://www.purrsandgurrs.com', 34000, NULL);
INSERT INTO `clg19`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Kitty Kingdom', '2251 GreenLeaf Dr.', 'Dayton', 'IA', 987343909, 4538675731, 'kittykingdomcontact@gmail.com', 'http://www.kittykingdom.com', 65700, NULL);
INSERT INTO `clg19`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Happy Paws', '8865 Amelia Circle', 'Orlando', 'FL', 909888277, 9067846500, 'happypaws@gmail.com', 'http://www.happypaws.com', 88500, NULL);
INSERT INTO `clg19`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Belly Rub Hub', '512 Monroe St.', 'Denver', 'CO', 332204876, 6887762316, 'bellyrubhub@gmail.com', 'http://www.bellyrubhub.com', 34000, NULL);
INSERT INTO `clg19`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Forever Friends', '87092 Colonial Dr.', 'Fort Collins', 'CO', 323040135, 7775634529, 'Bforeverfriends@yahoo.com', 'http://www.foreverfriends.org', 59000, NULL);
INSERT INTO `clg19`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pets and Pals', '6301 Henry Hill Ln', 'Miami', 'FL', 762238926, 9087594544, 'petandpals2@gmail.com', 'http://www.petandpals.com', 69700, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `clg19`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `clg19`;
INSERT INTO `clg19`.`customer` (`cust_id`, `cust_fname`, `cust_lname`, `cust_street`, `cust_city`, `cust_state`, `cust_zip`, `cust_phone`, `cust_email`, `cust_balance`, `cust_total_sales`, `cust_notes`) VALUES (DEFAULT, 'Tammy', 'Whitehouse', '502 Shady St.', 'Ashville', 'TN', 111903432, 4567370290, 'Whitehouse938@gmail.com', 200.01, 23009.55, NULL);
INSERT INTO `clg19`.`customer` (`cust_id`, `cust_fname`, `cust_lname`, `cust_street`, `cust_city`, `cust_state`, `cust_zip`, `cust_phone`, `cust_email`, `cust_balance`, `cust_total_sales`, `cust_notes`) VALUES (DEFAULT, 'Sam', 'Hudson', '19824 Mollywood Cir.', 'Houston', 'TX', 838495544, 1005939005, 'Shudson@gmail.com', 4589.26, 1356.23, NULL);
INSERT INTO `clg19`.`customer` (`cust_id`, `cust_fname`, `cust_lname`, `cust_street`, `cust_city`, `cust_state`, `cust_zip`, `cust_phone`, `cust_email`, `cust_balance`, `cust_total_sales`, `cust_notes`) VALUES (DEFAULT, 'Liz', 'Handler', '8278 Baldwin Dr.', 'Fort Collins', 'CO', 254904123, 3489375857, 'Lhandler@gmail.com', 139.52, 567.63, NULL);
INSERT INTO `clg19`.`customer` (`cust_id`, `cust_fname`, `cust_lname`, `cust_street`, `cust_city`, `cust_state`, `cust_zip`, `cust_phone`, `cust_email`, `cust_balance`, `cust_total_sales`, `cust_notes`) VALUES (DEFAULT, 'Jose', 'Franklin', '9009 Flowerfield St.', 'Ellisworth', 'WS', 283982481, 4829475633, 'Jfranklin@gmail.com', 37.82, 784.29, NULL);
INSERT INTO `clg19`.`customer` (`cust_id`, `cust_fname`, `cust_lname`, `cust_street`, `cust_city`, `cust_state`, `cust_zip`, `cust_phone`, `cust_email`, `cust_balance`, `cust_total_sales`, `cust_notes`) VALUES (DEFAULT, 'Pricilla', 'Mayden', '212 Northshore Dr.', 'Minneapolis', 'MN', 278471107, 8383948393, 'Mayden455@gmail.com', 10.00, 19203.60, NULL);
INSERT INTO `clg19`.`customer` (`cust_id`, `cust_fname`, `cust_lname`, `cust_street`, `cust_city`, `cust_state`, `cust_zip`, `cust_phone`, `cust_email`, `cust_balance`, `cust_total_sales`, `cust_notes`) VALUES (DEFAULT, 'Kennedy', 'French', '766 Washington Cir.', 'Bithlo', 'FL', 938347364, 2109092822, 'Kennedyemail@gmail.com', 0.00, 4500.12, NULL);
INSERT INTO `clg19`.`customer` (`cust_id`, `cust_fname`, `cust_lname`, `cust_street`, `cust_city`, `cust_state`, `cust_zip`, `cust_phone`, `cust_email`, `cust_balance`, `cust_total_sales`, `cust_notes`) VALUES (DEFAULT, 'Shyanne', 'Henry', '93836 Mirror Lake Dr.', 'Fort Christmas', 'FL ', 328486626, 1020068830, 'HenryShy@gmail.com', 2323.96, 7005.01, NULL);
INSERT INTO `clg19`.`customer` (`cust_id`, `cust_fname`, `cust_lname`, `cust_street`, `cust_city`, `cust_state`, `cust_zip`, `cust_phone`, `cust_email`, `cust_balance`, `cust_total_sales`, `cust_notes`) VALUES (DEFAULT, 'Frank', 'Ocean', '8380 Sun Park Blvd.', 'San Diego ', 'CA ', 875874309, 3284559505, 'Focean@yahoo.com', 213.73, 30928.90, NULL);
INSERT INTO `clg19`.`customer` (`cust_id`, `cust_fname`, `cust_lname`, `cust_street`, `cust_city`, `cust_state`, `cust_zip`, `cust_phone`, `cust_email`, `cust_balance`, `cust_total_sales`, `cust_notes`) VALUES (DEFAULT, 'Brandon', 'Troja', '322 Chiles Way', 'Tallahassee', 'FL', 463739828, 2345436491, 'Brandont@hotmail.com', 32.30, 12859.44, NULL);
INSERT INTO `clg19`.`customer` (`cust_id`, `cust_fname`, `cust_lname`, `cust_street`, `cust_city`, `cust_state`, `cust_zip`, `cust_phone`, `cust_email`, `cust_balance`, `cust_total_sales`, `cust_notes`) VALUES (DEFAULT, 'Lee', 'Hutchingson', '55493 Blue River St.', 'Atlanta ', 'GA', 102830222, 8948371044, 'Leehutch@gmail.com', 0.0, 6712.51, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `clg19`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `clg19`;
INSERT INTO `clg19`.`pet` (`pet_id`, `pst_id`, `cust_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 7, 4, 'Doberman', 'm', 350, 55, 2, 'black/tan', '2022-02-03', 'n', 'n', NULL);
INSERT INTO `clg19`.`pet` (`pet_id`, `pst_id`, `cust_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 8, NULL, 'Chiwawa', 'm', 175, 525, 4, 'white/brown', NULL, 'y', 'y', NULL);
INSERT INTO `clg19`.`pet` (`pet_id`, `pst_id`, `cust_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, 8, 'American Bobtail', 'm', 195, 165, 12, 'black', '2020-07-15', 'y', 'y', NULL);
INSERT INTO `clg19`.`pet` (`pet_id`, `pst_id`, `cust_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 4, 2, 'American Longhair', 'f', 165, 385, 6, 'white', '2021-11-01', 'y', 'n', NULL);
INSERT INTO `clg19`.`pet` (`pet_id`, `pst_id`, `cust_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 9, 1, 'British Shorthair', 'f', 155, 275, 5, 'grey', '2022-01-22', 'n', 'n', NULL);
INSERT INTO `clg19`.`pet` (`pet_id`, `pst_id`, `cust_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 8, 'Rose-Ringed Parakeet', 'm', 18, 145, 8, 'tan', '2019-05-19', 'y', 'y', NULL);
INSERT INTO `clg19`.`pet` (`pet_id`, `pst_id`, `cust_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, NULL, 'African Grey Parrot', 'f', 1295, 32, 13, 'spotted', NULL, 'n', 'n', NULL);
INSERT INTO `clg19`.`pet` (`pet_id`, `pst_id`, `cust_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, 9, 'Pitbull', 'f', 300, 1800, 2, 'grey/brown', '2021-12-30', 'y', 'y', NULL);
INSERT INTO `clg19`.`pet` (`pet_id`, `pst_id`, `cust_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 6, 5, 'Boa Constrictor', 'm', 450, 495, 1, 'orange', '2019-08-18', 'n', 'y', NULL);
INSERT INTO `clg19`.`pet` (`pet_id`, `pst_id`, `cust_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 2, 'Hamster', 'f', 1735, 735, 10, 'black', '2013-09-25', 'n', 'n', NULL);

COMMIT;

