import java.util.Scanner;


public class Methods{

    //Create Method without returning any value (without object)
    public static void getRequirements()
    {
     System.out.println("Developer: Corinna Gross");
     System.out.println("Program evalutes integer as even or odd.");
     System.out.println("Note: Program does *not* check for non-numeric characters.");
    }

    public static void evaluateNumber()
    {
        //initalize variables, create Scanner object, capture user input
        int num = 0;
        System.out.print("Enter integer: ");
        Scanner input = new Scanner(System.in);
        num = input.nextInt();

        if (num % 2 == 0)
        {
            System.out.print(num + " is an even number.");
        }else{
            System.out.print(num + " is an odd number.");
        }
    }
}