import java.util.Scanner;

public class Methods{
    public static void getRequirements(){
        System.out.println("Developer: Corinna Gross");
        System.out.println("Program evalutates largest of two integers.");
        System.out.println("Note: Program does *not* check for non-numeric characters of non-integer values");
        System.out.println(); //prints blank line
    }

    //nonvalue-returning method (without object - static)
    public static void largestNumber()
    {
        //declare variables and create Scanner object
        int num1, num2;
        Scanner input = new Scanner(System.in);

        System.out.print("Enter first integer: ");
        num1 = input.nextInt();

        System.out.print("Enter second integer: ");
        num2 = input.nextInt();

        System.out.println(); 
        if(num1 > num2)
        System.out.println(num1 + " is larger than " + num2);
        else if(num2 > num1)
        System.out.println(num2 + " is larger than " + num1);
        else
        System.out.println("Integers are equal."); 
    }

    //value-returning method(without object -static)
    public static int getNum()
    {
        Scanner input = new Scanner(System.in);
        return input.nextInt();
    }

    //nonvalue-returning method (without object - static)
    public static void evaluateNumber(int num1, int num2)
    {
        System.out.println();
        if(num1 > num2)
        System.out.println(num1 + " is larger than " + num2);
        else if(num2 > num1)
        System.out.println(num2 + " is larger than " + num1);
        else
        System.out.println("Integers are equal."); 
        return;
    }

}