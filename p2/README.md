# Lis4381 Mobile WebApp Development

## Corinna Gross

### Lis4381 Requirements:

*Three Parts:*

1. Project 2 server-side validation
    - Screenshots of portfolio home page
    - Screenshots of P2 main page
    - Screenshot of P2 failed server-side validation
    - Screenshot of P2 passed server-side validation
2. Project 2 edit/delete functionality
    - Screenshot of successful delete record
3. Project 2 RSS feed


#### Online Portfolio: Project 2 Server-side data validation

*Portfolio Main Page Carousel:
![Portfolio Main Page Carousel](img/p2_carousel.png)


| *Screenshot of Project 2 Index: | *Screenshot of Edit Petstore:     |
|:--------------------------------|---------------------------------|
![Failed Data Validation Screenshot](img/p2_index_ss.png) | ![Passed Data Validation Screenshot](img/p2_edit_petstore.png) 


| *Screenshot of Failed Server-side Validation: | *Screenshot of Successful Server-side Validation:     |
|:--------------------------------|---------------------------------|
![Failed Data Validation Screenshot](img/p2_failed_validation.png) | ![Passed Data Validation Screenshot](img/p2_working_edit.png) 



#### Project 2 edit/delete functionality

| *Screenshot of Delete Record Prompt: |  *Screenshot of Successfully Deleted Record: |
|:--------------------------------|---------------------------------|
![Delete Record Prompt Screenshot](img/p2_delete_record_prompt.png) | ![Successfully Deleted Record Screenshot](img/p2_deleted_record.png) 

#### Project 2 RSS Feed

*RSS feed (Link to RSS Feed of our choice):
![RSS Feed](img/p2_rss.png)
