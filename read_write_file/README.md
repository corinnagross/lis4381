# Lis4381 Mobile WebApp Development

## Corinna Gross

### Lis4381 Requirements:

*Three Parts:*

1. Screenshots of Online Portfolio
    - Screenshots of websites main page
    - Screenshots of A4 client-side data validation
2. Screenshot of Skillsets


#### Online Portfolio (Main Page):

*Screenshot of Portfolio Main Page 

![Online Porfolio Main Page](img/portfolio_main.png)

#### Online Portfolio: Data Validation

| *Screenshot of Failed Data Validation: | *Screenshot of Successful Data Validation: |
|:--------------------------------|---------------------------------|
![Failed Data Validation Screenshot](img/failed_validation.png) | ![Successful Data Validation Screenshot](img/passed_validation.png) 



#### Skillset Screenshots:

| *Screenshot of Skillset 10 - Array List: | *Screenshot of Skillset 11 - Alpha Numeric Special: | *Screenshot of Skillset 12 - Temperature Conversion: |
| ------------------------------------------------ | ---------------------------------------------------- | ------------------------------------ |
| ![Skillset 10 Screenshot](img/Q10_screenshot.png) | ![Skillset 11 Screenshot](img/Q11_screenshot.png) | ![Skillset 12 Screenshot](img/Q12_screenshot.png) |
