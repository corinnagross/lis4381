<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
//exit(print_r($_POST));

//$comments = trim($_POST['comments']);

//exit(print_r($_POST)); //display $_POST array values from form

// or, for nicer display in browser...
 //echo "<pre>";
 //print_r($_POST);
 //echo "</pre>";
 //exit(); //stop processing, otherwise, errors below



//include('index.php'); //forwarding is faster, one trip to server
//header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)


?> 
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta https-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="An online portfolio that highlights some of my projects and skills aquired relating to Information, Communication, and Technology.">
	    <meta name="author" content="Corinna Gross">
	    <link rel="icon" href="favicon.ico">

        <title>LIS4381 - Read/Write File</title>
        <?php include_once("../css/include_css.php"); ?>
   </head>

 <body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>

                <h3>Program Writes to and Reads From Same File</h3>
					
				<h2>File Data</h2>
                <p class="text-justify">
            <?php


           //create/open file for appending write/read

            $myfile = fopen("file.txt","w+") or exit("Unable to open file!");
            $txt = $_POST['comment'];
            fwrite($myfile, $txt);
            fclose($myfile);

            $myfile = fopen("file.txt","r+") or exit("Unable to open file!");
            while(!feof($myfile)){
                echo fgets($myfile) . "<br/>";
            }
            fclose($myfile);
            ?>
            </p>

            <?php include_once "global/footer.php"; ?>
        
       </div><!--starter template-->
    </div><!--end container-->

    <!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
<?php include_once("../js/include_js.php"); ?>
*<script>
$(document).ready(function(){
	$('#myTable').DataTable({
		responsive: true
	});
});
</script>

</body>
</html>

